1. From the Safari toolbar, click the **Adblock Plus** icon and select **Open Adblock Plus**.
<br>The *Adblock Plus Settings* window opens.
2. Clear the check box labeled **Allow Acceptable Ads**.
3. Close the tab.
