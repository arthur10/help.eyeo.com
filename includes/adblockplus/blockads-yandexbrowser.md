1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
4. From the *General* tab, scroll to the Acceptable Ads section.
5. Clear the check box labeled **Allow Acceptable Ads**.
6. Close the tab.
