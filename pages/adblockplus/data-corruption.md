title=Fix Chrome, Firefox, and Opera data corruption issue
description=There might be an instance when a user's ABP settings are reset due to a browser-related data corruption issue. Adblock Plus must be uninstalled and reinstalled.
template=article
product_id=abp
category=Troubleshooting & Reporting

A browser issue has caused your ABP settings to be reset.

Because of this, your ABP settings have been reset to default. To resolve the issue, you'll need to uninstall and reinstall Adblock Plus. Follow the steps below to do this. Note that depending on how you use ABP, you may need to re-configure your settings, custom filters, and whitelisted websites once you reinstall Adblock Plus.

<section class="platform-chrome" markdown="1">
## Chrome

### How to uninstall Adblock Plus...

1. From the Chrome toolbar, click the **Chrome menu** icon, hover over **More Tools** and select **Extensions**.
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and click **Remove**.
<br>A dialog box opens.
3. Click **Remove**.
4. Close the tab.

### How to reinstall Adblock Plus...

1. Open Chrome and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Chrome**.
<br>A dialog box opens.
3. Click **Add extension**.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

### How to uninstall Adblock Plus...

1. Click the **Firefox menu** icon and select **Add-ons**.
<br>The *Add-ons Manager* tab opens.
2. Select **Extensions** from the left menu.
3. Locate Adblock Plus, click **...** and then click **Remove**.
4. Click **Remove**.
5. Close the tab.

### How to reinstall Adblock Plus...

1. Open Firefox and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Firefox**.
<br>A dialog box opens.
3. Click **Allow**.
<br>A dialog box opens.
4. Click **Add**.
</section>

<section class="platform-opera" markdown="1">
## Opera

### How to uninstall Adblock Plus...

1. From the Opera toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extension**.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Extensions* tab opens.
2. Locate Adblock Plus and click **Remove extension**.
<br>A dialog box opens.
3. Click **Remove**.
4. Close the tab.

### How to reinstall Adblock Plus...

Adblock Plus for Opera must be installed via Opera add-ons.

1. Open Opera and go to [AdblockPlus.org](https://adblockplus.org).
2. Click **Agree and install for Opera**.
<br>The *Opera add-ons* page opens.
3. Click **Add to Opera**.
</section>
