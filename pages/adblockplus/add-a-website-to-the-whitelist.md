title=Add a website to the whitelist
description=Add websites that you trust and want to support to your Adblock Plus whitelist. Ads will be shown on these websites.
template=article
product_id=abp
category=Customization & Settings

In general, a whitelist is a list of items that has been approved to receive special privileges. You can add website URLs to a whitelist in order to view ads on websites that you want to support.

<aside class="alert info" markdown="1">
**Important**: All ads, including those that do not adhere to the Acceptable Ads criteria, will be shown on whitelisted websites.
</aside>

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Whitelisted websites* tab.
3. Enter the website URL and click **Add website**.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Whitelisted websites* tab.
3. Enter the website URL and click **Add website**.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Whitelisted websites* tab.
3. Enter the website URL and click **Add website**.
4. Close the tab.
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

1. From the status bar located at the bottom of the browser, click the **Adblock Plus** icon and select **Settings**.
<aside class="alert info" markdown="1">
**Tip**: If you do not see the Adblock Plus icon, right-click the tab bar (the empty area near the tabs) and select **Status bar**.
</aside>
The *Adblock Plus Options* tab opens.
2. Click **Manage**.
3. Enter the website URL and click **Add**.
4. Close the tab.
</section>

<section class="platform-ios" markdown="1">
## Adblock Plus for iOS

1. Open the Adblock Plus for iOS app.
2. Tap the **Tools** icon.
3. Tap **Whitelisted Websites**.
4. Tap the field below *Add a Website to the Whitelist*.
5. Enter the website URL and tap the **+ sign**.
6. Close the app.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Whitelisted websites* tab.
3. Enter the website URL and click **Add website**.
4. Close the tab.
</section>

<section class="platform-safari" markdown="1">
## Safari

1. From the Safari toolbar, click the **Adblock Plus** icon and select **Open Adblock Plus**.
<br>The *Adblock Plus Settings* window opens.
2. Select the *Whitelist* tab.
3. Enter the website URL and click **Add Website**.
4. Close the window.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Whitelisted websites* tab.
3. Enter the website URL and click **Add website**.
4. Close the tab.
</section>
