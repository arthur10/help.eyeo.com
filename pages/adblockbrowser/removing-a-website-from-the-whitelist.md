title=Remove a website from the whitelist
description=No longer want to allow ads on websites that you previously whitelisted? Remove the websites from your Adblock Browser whitelist.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings
include_heading_level=h2

Follow the steps below if you added a website to the Adblock Browser whitelist, but now want to remove it.

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Ad blocking**.
4. Tap **Whitelisted domains**.
5. Tap the **-** icon next to the website that you want to remove.
